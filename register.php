<?php
 
 //Memanggil koneksi.php yang telah kita buat sebelumnya
 include "includes/koneksi.php";
 
 //this is our upload folder 
 $upload_path = 'uploads/';
 
 //Getting the server ip 
 //$server_ip = gethostbyname(gethostname());
 
 //creating the upload url 
 $upload_url = 'http://penampungaspirasi.com/api/'.$upload_path; 

// Respon json yaitu bentuk array
$response = array("status" => TRUE);
 
if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['nama_lengkap']) && isset($_POST['alamat']) && isset($_POST['no_telp']) && isset($_FILES['foto'])) {
 
    // Menerima parameter yg di kirimkan
    $username = $_POST['username'];    
    $password = $_POST['password'];
    $nama_lengkap = $_POST['nama_lengkap'];
    $alamat = $_POST['alamat'];
    $no_telp = $_POST['no_telp'];
    //$foto = $_POST['foto'];

    /**
     * Cek user sudah ada atau tidak
     */
    function isUserExisted($username) {
        $sql = "SELECT username from users WHERE username = '$username' ";
        $query = mysql_query($sql);
 
        if (mysql_num_rows($query) > 0) {
            // User sudah ada
            return true;
        } else {
            // User tidak ada
            return false;
        }
    }

    //getting file info from the request 
    $fileinfo = pathinfo($_FILES['foto']);

    //getting the file extension 
    $extension = $fileinfo['extension'];
     
    //file url to store in the database 
    $file_url = $upload_url . date("Y-m-d-h-i") . '.png';
     
    //file path to upload in the server 
    $file_path = $upload_path . date("Y-m-d-h-i") . '.png';
 
    // Cek apakah username sudah ada di database
    if (isUserExisted($username)) {
        // Username sudah ada di database
        $response["status"] = FALSE;
        $response["msg"] = "Username $username sudah terdaftar, ganti dengan yang lain ";
        echo json_encode($response);
    } else {
        // Membuat user baru
        //saving the file 
        move_uploaded_file($_FILES['foto']['tmp_name'],$file_path);

        //level default ketika registrasi
        $level = "4"; // 1 = admin, 2 = kepdes, 3 = perdes, 4 = masyarakat
        $sql = "INSERT INTO users(level, username, password, nama_lengkap, alamat, no_telp, foto) VALUES ('$level', '$username', '$password', '$nama_lengkap', '$alamat', '$no_telp', '$file_url')";
        $query = mysql_query($sql);
        if ($query) {
            // User berhasil disimpan dan mendapatkan detail user

            $response["status"] = TRUE;
            $response["msg"] = "Username $username berhasil di daftarkan";
            echo json_encode($response);
        } else {
            // Terjadi kesalahan ketika menyimpan user baru
            $response["status"] = FALSE;
            $response["msg"] = "Terjadi kesalahan !";
            echo json_encode($response);
        }
    }
} else {
    $response["status"] = FALSE;
    $response["msg"] = "Data belum terisi semua !";
    echo json_encode($response);
}


?>