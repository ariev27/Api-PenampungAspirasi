<?php
 //Memanggil conn.php yang telah kita buat sebelumnya
 include "includes/koneksi.php";

  //Folder upload
  $upload_path = 'uploads/';
  
  //Upload url
  $upload_url = 'http://penampungaspirasi.com/api/'.$upload_path;

  if (isset($_POST['id_pengaduan']) && isset($_POST['id_user']) && isset($_POST['id_kategori']) && isset($_POST['pengaduan']) && isset($_POST['tgl_pengaduan']) && isset($_FILES['foto'])) {

    // Menerima parameter yg di kirimkan
    $id_pengaduan = $_POST['id_pengaduan'];
    $id_user = $_POST['id_user'];
    $id_kategori = $_POST['id_kategori'];
    $pengaduan = $_POST['pengaduan'];
    $tgl_pengaduan = $_POST['tgl_pengaduan'];

    //Mengambil informasi file dari request yang di kirimkan
    $fileinfo = pathinfo($_FILES['foto']);
     
    //Url file yang akan di simpan ke database
    $file_url = $upload_url . 'pengaduan-' . date("Y-m-d-h-i") . '.png';
     
    //File yang akan di upload ke server
    $file_path = $upload_path . 'pengaduan-' . date("Y-m-d-h-i") . '.png';

    //Menyimpan file ke folder uploads
    move_uploaded_file($_FILES['foto']['tmp_name'],$file_path);
 
     //Syntax MySql untuk menyimpan pengaduan
     $sql = "INSERT INTO pengaduan(id_pengaduan, id_user, id_kategori, pengaduan, tgl_pengaduan, foto) VALUES('$id_pengaduan', '$id_user', '$id_kategori', '$pengaduan', '$tgl_pengaduan', '$file_url')";
      
     //Execute Query diatas
     $query = mysql_query($sql);
     if ($query) {
       $json ['result'] = TRUE;
       $json ['msg'] = 'Pengaduan berhasil di kirim';

       echo json_encode($json);
     } else {
      $json ['result'] = FALSE;
      $json ['msg'] = 'Pengaduan gagal di kirim';

      echo json_encode($json);
     }

  } else {
    $json ['result'] = FALSE;
    $json ['msg'] = 'Parameter belum terisi semua';

    echo json_encode($json);
  }

?>
